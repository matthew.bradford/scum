extern crate bzip2;
extern crate dirs;
extern crate fetch_file;
extern crate rayon;
#[macro_use]
extern crate serde;
extern crate tar;
extern crate zip;

use std::fs::File;
use std::path::PathBuf;

use bzip2::read::BzDecoder;
use std::io::Read;
use tar::Archive;

pub mod game;
pub mod scum;

fn unzip_file(source: &PathBuf, dest: &PathBuf) -> std::io::Result<()> {
    if source.is_file() {
        let ext = source.extension().unwrap().to_str().unwrap();
        match ext {
            "zip" => zip(source, dest)?,
            "bz2" => bziptar(source, dest)?,
            _ => println!("Extension not supported"),
        }
    }

    Ok(())
}

fn bziptar(source: &PathBuf, dest: &PathBuf) -> std::io::Result<()> {
    let file = std::fs::File::open(source).unwrap();
    let dcon = BzDecoder::new(file);
    let mut tball = Archive::new(dcon);

    match tball.unpack(dest) {
        Ok(_) => println!("success"),
        Err(e) => println!("Error: {:?}", e),
    }

    Ok(())
}

fn zip(source: &PathBuf, dest: &PathBuf) -> std::io::Result<()> {
    let mut file = vec![];
    File::open(source).unwrap().read_to_end(&mut file)?;
    let reader = std::io::Cursor::new(file);
    let mut zip = zip::ZipArchive::new(reader).expect("Failed to read zip");

    for i in 0..zip.len() {
        let mut file = zip.by_index(i).unwrap();
        let mut outpath = dest.to_path_buf();
        outpath.push(file.sanitized_name());

        if (file.name()).ends_with('/') {
            std::fs::create_dir_all(&outpath).unwrap();
        } else {
            if let Some(p) = outpath.parent() {
                if !p.exists() {
                    std::fs::create_dir_all(&p).unwrap();
                }
            }
            let mut outfile = std::fs::File::create(&outpath).unwrap();
            std::io::copy(&mut file, &mut outfile).unwrap();
        }
    }

    Ok(())
}

/// #
///
/// Request loop for user enter y/n.
///
/// # Returns
///  * true if y/Y
///  * false n/N
///
///
// might have to re-think this later if a GUI is created
// to use scum (i plan to).
pub fn prompt(msg: &str) -> Option<bool> {
    loop {
        println!("{} y/n/q", msg);
        let mut input = String::new();
        match std::io::stdin().read_line(&mut input) {
            Ok(_) => (),
            Err(e) => println!("Error: {:?}", e),
        }

        let input = input.trim().to_lowercase();
        match input.as_str() {
            "y" => return Some(true),
            "n" => return Some(false),
            "q" => return None,
            _ => {
                println!("Invalid input");
                continue;
            }
        }
    }
}
