use std::error::Error;
use std::path::PathBuf;

use fetch_file::Fetchable;
use serde::de::DeserializeOwned;
use std::process::Command;

pub const GAME_CONFIG_NAME: &str = "game.config";

/// #
///
/// Config specific to each game for settings things like archiving behavior and paths to game save and scum archives.
///
/// # Fields
///  * game_save_path: Directory where game saves its state
///  * scum_archive: Directory where save archives
///  * scum_runner: Default to sh/cmd. Wine would be another common one.
///  * retention_count: Number of archives Scum will create before it starts writing over old ones
///
#[derive(Clone, Debug, Deserialize, Serialize)]
pub struct GameConfig {
    game_save_path: Option<PathBuf>,
    game_executable: Option<PathBuf>,
    scum_archive: Option<PathBuf>,
    scum_runner: Option<PathBuf>,
    retention_count: usize,
}

impl Default for GameConfig {
    fn default() -> GameConfig {
        GameConfig {
            game_save_path: dirs::home_dir(),
            game_executable: None,
            scum_archive: dirs::cache_dir(),
            scum_runner: None,
            retention_count: 0,
        }
    }
}

impl Fetchable for GameConfig {
    fn deserialize_l<T: DeserializeOwned + Default + Fetchable>(
        f_path: &PathBuf,
    ) -> Result<T, Box<dyn Error>> {
        GameConfig::deserialize_ron(f_path)
    }

    fn serialize_l(&self) -> Result<Vec<u8>, Box<dyn Error>>
    where
        Self: serde::Serialize + Fetchable,
    {
        self.serialize_ron()
    }
}

impl GameConfig {
    /// #
    ///
    /// new -
    ///
    /// # Parameters
    ///  * game_save_path: path to games save directory
    ///  * scum_archive: directory where game saves are archived
    ///  * scum_runner: optional runner like wine
    ///  * count: fills retention_count and limits the number of game save archives that will be kept
    pub fn new(
        game_save_path: Option<PathBuf>,
        scum_archive: Option<PathBuf>,
        scum_runner: Option<PathBuf>,
        count: usize,
    ) -> GameConfig {
        let mut config = GameConfig::default();
        config.game_save_path = game_save_path;
        config.scum_archive = scum_archive;
        config.scum_runner = scum_runner;
        config.retention_count = count;
        config
    }

    pub fn run(&self) -> std::io::Result<Option<u32>> {
        println!("Starting game");
        let mut pid = None;
        if self.game_executable.is_none() {
            println!("Game executable path not set");
        }

        if self.scum_runner.is_some() {
            let child = Command::new(self.scum_runner.as_ref().unwrap())
                .arg(self.game_executable.as_ref().unwrap())
                .arg("&")
                .spawn()
                .expect("buggers");

            pid = Some(child.id());
        }

        Ok(pid)
    }
}
