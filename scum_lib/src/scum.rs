use std::collections::HashMap;
use std::error::Error;
use std::path::PathBuf;

use fetch_file::Fetchable;
use serde::de::DeserializeOwned;

use crate::game::{GameConfig, GAME_CONFIG_NAME};
use crate::*;

pub const SCUM_CONFIG_NAME: &str = "scummy.config";

/// #
///
/// Main config for Scum. This will contain a list of games that have been installed.
/// A list of common game names so that downloaded archives of the games can be
/// installed quickly without typing out paths.
///
/// # Fields
///  * download_dir: Default download directory. Set this if you want to pass commands to install a game from downloaded archive without typing out paths.
///  * games: Map of installed/mapped games.
///  * last_played: will be used for something like scum -l to start up the last game you played with Scum.
///
#[derive(Clone, Debug, Deserialize, Serialize)]
pub struct ScumConfig {
    download_dir: PathBuf,
    games: HashMap<String, SysGame>,
    process: HashMap<String, u32>,
    games_external: Vec<Game>,
    last_played: Option<String>,
}

/// #
///
/// Game struct
///
/// # Fields
///  * name: Long name for the game.
///  * path: Install directory for the game.
///  * signature: Download archive file name signature. ex) Dwarf fort files are df_<major version>_<minor>.zip signature is df_.
///
#[derive(Clone, Debug, Deserialize, Serialize)]
pub struct SysGame {
    name: Option<String>,
    path: Option<PathBuf>,
    signature: Option<String>,
}

impl Default for SysGame {
    fn default() -> SysGame {
        SysGame {
            name: None,
            path: None,
            signature: None,
        }
    }
}

impl SysGame {
    fn new(name: Option<&str>, path: &str, signature: Option<&str>) -> SysGame {
        let mut game = SysGame::default();
        game.name = if name.is_some() {
            Some(name.unwrap().to_string())
        } else {
            None
        };
        game.path = Some(PathBuf::from(path));
        game.signature = if signature.is_some() {
            Some(signature.unwrap().to_string())
        } else {
            None
        };
        game
    }
}

#[derive(Clone, Debug, Deserialize, Serialize)]
pub struct Game {
    name: String,
    website: String,
}

impl Game {
    fn new(name: &str, website: &str) -> Game {
        Game {
            name: String::from(name),
            website: String::from(website),
        }
    }
}

impl Default for ScumConfig {
    fn default() -> ScumConfig {
        ScumConfig {
            download_dir: PathBuf::new(),
            games: HashMap::new(),
            process: HashMap::new(),
            games_external: my_favorite_games(),
            last_played: None,
        }
    }
}

fn my_favorite_games() -> Vec<Game> {
    let mut external_list = vec![];
    external_list.push(Game::new("Angband", "https://rephial.org/"));
    external_list.push(Game::new(
        "Elona Plus",
        "https://elona.fandom.com/wiki/Elona_Wiki",
    ));
    external_list.push(Game::new(
        "Dungeon Crawl Stone Soup",
        "https://crawl.develz.org/download.htm",
    ));
    external_list.push(Game::new(
        "Net Hack",
        "https://nethack.org/common/index.html",
    ));

    external_list
}

impl Fetchable for ScumConfig {
    fn deserialize_l<T>(f_path: &PathBuf) -> Result<T, Box<dyn Error>>
    where
        T: DeserializeOwned + Default + Fetchable,
    {
        ScumConfig::deserialize_bin(f_path)
    }

    fn serialize_l(&self) -> Result<Vec<u8>, Box<dyn Error>>
    where
        Self: serde::Serialize + Fetchable,
    {
        self.serialize_bin()
    }
}

/// #
///
/// Get the default Scum config
///
pub fn get_config() -> ScumConfig {
    let execute_dir = default_config_fp();
    let config: (ScumConfig, bool) = ScumConfig::fetch_or_default(&execute_dir).unwrap();

    if config.1 {
        match config.0.save() {
            Ok(_) => println!("Config saved"),
            Err(e) => println!("Failed to save config: {:?}", e),
        }
    }
    config.0
}

/// #
///
/// Get the default path to main Scum config.
///
pub(crate) fn default_config_fp() -> PathBuf {
    use dirs::config_dir;
    let mut pb = config_dir().expect("Unable to locate config directory");
    pb.push("scummy");
    if !pb.exists() {
        match std::fs::create_dir_all(&pb) {
            Ok(_) => (),
            Err(e) => panic!("Failed to create directory: {:?}", e),
        }
    }
    pb.push(SCUM_CONFIG_NAME);
    pb
}

impl ScumConfig {
    /// #
    ///
    /// Add game to config. Call init name to generate game config and scum archive.
    ///
    /// # Parameters
    ///  - name: Display name for the game.
    ///  - id: An identifier to make the game easy to work with through scum.
    ///  - path: Location of game directory and where scum will add a scum game config.
    ///  - sig: Download file signature.
    ///
    pub fn add(
        &mut self,
        id: Option<&str>,
        name: Option<&str>,
        path: Option<&str>,
        sig: Option<&str>,
    ) -> std::io::Result<bool> {
        let mut save = false;
        if id.is_some() && path.is_some() {
            let id = id.unwrap();
            let path = path.unwrap();
            match self
                .games
                .insert(id.to_string(), SysGame::new(name, path, sig))
            {
                Some(old) => (println!("Replaced {:?}, with {} for {})", old, path, id)),
                None => println!("new game {}, {} added", id, path),
            }
            save = true;
        } else {
            println!("Id and Path required.");
        }
        Ok(save)
    }

    /// #
    ///
    /// Check if the map of games contains a value for this key already.
    ///
    /// # Parameters
    ///  * name: id used to make games easier to interact with through Scum.
    ///
    pub fn contains(&self, name: &str) -> bool {
        self.games.contains_key(name)
    }

    /// #
    ///
    /// Add downloads directory to Scum.
    ///
    /// # Parameters
    ///  * path: Path to directory games are downloaded to.
    ///
    pub fn downloads(&mut self, path: Option<&str>) -> std::io::Result<bool> {
        if path.is_none() {
            println!("{:?}", self.download_dir);
            return Ok(false);
        }
        let path = path.unwrap();
        self.download_dir = PathBuf::from(path);

        if self.download_dir.is_dir() && self.download_dir.exists() {
            println!("Downloads path set to: {:?}", self.download_dir);
            return Ok(true);
        } else {
            println!("Invalid path");
            return Ok(false);
        }
    }

    /// #
    ///
    /// My favorite games
    ///
    pub fn favs(&self) -> &Vec<Game> {
        for i in &self.games_external {
            println!("{} {}", i.name, i.website);
        }

        &self.games_external
    }

    /// #
    ///
    /// Install
    ///
    /// # Parameters
    ///  - id: An identifier to make the game easy to work with through scum.
    ///
    pub fn install(&self, id: Option<&str>) -> std::io::Result<bool> {
        let mut save = false;
        if id.is_some() {
            let id = id.unwrap();
            let game = self.games.get(id);
            if game.is_some() {
                let game = game.unwrap();
                if game.signature.is_some() {
                    let sig = game.signature.as_ref().unwrap();
                    let m = get_install_by_pattern(&self.download_dir, sig)?;
                    if m.is_some() && game.path.is_some() {
                        let source = m.unwrap();
                        let mut target = game.path.as_ref().unwrap().to_path_buf();
                        crate::unzip_file(&source, &target)?;
                        target.push(GAME_CONFIG_NAME);
                        let config: (GameConfig, bool) = GameConfig::fetch_or_default(&target)
                            .expect("Failed to create game config");
                        if config.1 {
                            config.0.save(&target).expect("Failed to save config");
                        }
                        save = true;
                    } else {
                        println!("Missing game path or signature");
                    }
                } else {
                    println!("Game file signature required");
                }
            } else {
                println!("Game id not found");
            }
        } else {
            println!("Game id required");
        }
        Ok(save)
    }

    /// #
    ///
    /// List of games with path
    ///
    pub fn list(&self) -> Vec<(&str, &SysGame)> {
        println!("Scummed Games: ");
        let mut games = vec![];
        for (k, v) in &self.games {
            println!("{:?}, {:?}, {:?}, {:?}", k, v.name, v.path, v.signature);
            games.push((k.as_str(), v))
        }
        games
    }

    /// #
    ///
    /// Remove
    ///
    /// # Parameters
    ///  - id: An identifier to make the game easy to work with through scum.
    ///  - del_config: Remove should delete config.
    ///
    pub fn remove(&mut self, id: Option<&str>, del_config: Option<bool>) -> std::io::Result<bool> {
        // quit out of prompt do nothing.
        if del_config.is_none() {
            return Ok(false);
        }
        let del_config = del_config.unwrap();

        let mut save = false;
        if id.is_some() {
            let id = id.unwrap();
            match self.games.remove(id) {
                Some(removed) => {
                    if del_config && removed.path.is_some() {
                        remove_config(&removed.path.unwrap());
                    }
                    // regardless of success deleting config we want to save
                    // user will have to manually delete and look at error to correct
                    // issue in future.
                    save = true;
                }
                None => println!("{} not found", id),
            }
        } else {
            println!("Name required");
        }
        Ok(save)
    }

    /// #
    ///
    /// # Run
    ///
    /// Run a game using the runner option or default system.
    ///
    pub fn run(&self, id: Option<&str>) -> std::io::Result<Option<u32>> {
        if id.is_some() {
            let id = id.unwrap();
            let game_config = self.games.get(id).expect("Game not found").clone();
            let mut path = game_config.path.expect("Game path not set");
            path.push(GAME_CONFIG_NAME);
            let game: (GameConfig, bool) =
                GameConfig::fetch_or_default(&path).expect("Failed to fetch game config");
            if game.1 {
                game.0.save(&path).expect("Failed to save game config");
            }
            Ok(game.0.run()?)
        } else {
            println!("Game id is required");
            Ok(None)
        }
    }

    /// #
    ///
    /// Save the config to disk
    ///
    pub fn save(&self) -> std::io::Result<()> {
        let path = default_config_fp();
        Fetchable::save(self, &path).expect("Failed to save");
        Ok(())
    }

    /// #
    ///
    /// Update a game entry by id
    ///
    /// # Parameters
    ///  - name: Display name for the game.
    ///  - id: An identifier to make the game easy to work with through scum.
    ///  - path: Location of game directory and where scum will add a scum game config.
    ///  - sig: Download file signature.
    ///
    pub fn update(
        &mut self,
        id: Option<&str>,
        name: Option<&str>,
        path: Option<&str>,
        sig: Option<&str>,
    ) -> std::io::Result<bool> {
        if id.is_some() {
            let id = id.unwrap();
            let game = self.games.get_mut(id).expect("Failed to find game");
            if name.is_some() {
                game.name = Some(name.unwrap().to_string());
            }
            if path.is_some() {
                game.path = Some(PathBuf::from(path.unwrap()));
            }
            if sig.is_some() {
                game.signature = Some(sig.unwrap().to_string());
            }
        }

        Ok(true)
    }

    pub fn set_current_process_id(
        &mut self,
        id: Option<&str>,
        pid: Option<u32>,
    ) -> std::io::Result<bool> {
        if id.is_some() && pid.is_some() {
            self.process.insert(id.unwrap().to_string(), pid.unwrap());
            Ok(true)
        } else {
            Ok(false)
        }
    }

    pub fn kill(&mut self, id: Option<&str>) -> std::io::Result<bool> {
        if id.is_some() {
            let id = id.unwrap();
            let pid = self.process.get(id);
            if pid.is_some() {
                let pid = *pid.unwrap();
                std::process::Command::new("kill")
                    .arg(format!("{}", pid))
                    .output()
                    .expect("Failed to kill process.");

                self.process.remove(id);
                Ok(true)
            } else {
                println!("No process with that game id");
                Ok(false)
            }
        } else {
            Ok(false)
        }
    }
}

fn directory_list(dwnloads: &PathBuf) -> std::io::Result<Vec<PathBuf>> {
    let mut list = vec![];
    if dwnloads.is_dir() {
        for entry in std::fs::read_dir(dwnloads)? {
            let entry = entry?;
            let path = entry.path();

            if path.is_file() {
                list.push(path);
            }
        }
    }

    Ok(list)
}

fn get_install_by_pattern(dwnlds: &PathBuf, sig: &str) -> std::io::Result<Option<PathBuf>> {
    let list = directory_list(dwnlds)?;
    let filter = filter_by_pattern(sig, list)?;
    Ok(get_latest_dnld(filter)?)
}

fn get_latest_dnld(filter: Vec<PathBuf>) -> std::io::Result<Option<PathBuf>> {
    let mut greatest = None;
    let mut sum = 0;
    for path in filter {
        let fname = path
            .file_name()
            .expect("Failed to get file name")
            .to_str()
            .expect("File name result None");
        let mut sum_string = 0;
        for c in fname.chars().rev() {
            if c.is_numeric() {
                sum_string += c as i32;
            }
        }

        if sum_string >= sum {
            sum = sum_string;
            greatest = Some(path.to_path_buf());
        }
    }

    Ok(greatest)
}

fn filter_by_pattern(sig: &str, dwnlds: Vec<PathBuf>) -> std::io::Result<Vec<PathBuf>> {
    let mut sig_matches = vec![];

    for d in dwnlds {
        if d.is_file() {
            let file_name = d.file_name().unwrap().to_str().unwrap().to_string();
            if file_name.contains(sig) {
                sig_matches.push(d.to_path_buf());
            }
        }
    }

    Ok(sig_matches)
}

pub fn remove_config(config_path: &PathBuf) {
    let mut game_config = config_path.to_path_buf();
    game_config.push(crate::game::GAME_CONFIG_NAME);
    match std::fs::remove_file(&game_config) {
        Ok(_) => println!("Removed config: {:?}", game_config),
        Err(e) => println!("Failed to remove config: {:?}, error: {:?}", game_config, e),
    }
}

#[cfg(test)]
mod scum_tests {
    use super::*;

    #[test]
    fn remove_not_found() {
        let mut config = ScumConfig::default();
        config.add(Some("duck"), None, Some("/path/to/game"), None);
        let save = config.remove(Some("duck2"), Some(false)).unwrap();
        assert!(!save, "remove config.remove failed to set save flag");
    }

    #[test]
    fn remove() {
        let mut config = ScumConfig::default();
        config.add(Some("duck"), None, Some("/path/to/game"), None);
        let save = config.remove(Some("duck"), Some(false)).unwrap();
        assert!(save, "remove config.remove failed to set save flag");
    }

    #[test]
    fn list() {
        let mut config = ScumConfig::default();
        config.add(Some("duck"), None, Some("path/to/game"), None);
        let list = config.list();
        let first_tup = list.get(0).unwrap();
        assert!(first_tup.0 == "duck");
    }

    #[test]
    fn add() {
        let mut config = ScumConfig::default();
        let save = config
            .add(
                Some("duck"),
                Some("long duck name"),
                Some("/path/to/duck"),
                Some("duck_"),
            )
            .unwrap();
        assert!(save, "add config.add failed to set save flag");
        assert!(config.contains("duck") == true);
    }

    #[test]
    fn add_missing_name() {
        let mut config = ScumConfig::default();
        config.add(None, Some("home/duck/duck"), None, None);
        assert!(config.contains("duck") == false);
    }

    #[test]
    fn add_missing_path() {
        let mut config = ScumConfig::default();
        config.add(Some("name"), None, None, None);
        assert!(config.contains("duck") == false);
    }

    #[test]
    fn add_missing_params() {
        let mut config = ScumConfig::default();
        config.add(None, None, None, None);
        assert!(config.contains("duck") == false);
    }
}
