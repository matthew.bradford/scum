extern crate clap;
extern crate scum_lib;

use clap::{App, Arg};
use scum_lib::{prompt, scum::get_config};

fn main() -> std::io::Result<()> {
    let matches = App::new("Scum")
        .version("0.1.0")
        .author("Matthew Bradford")
        .about(
            "Tool for dungeon crawlers that hard core peeps will hate. Supports .zip and .tar.bz2 \
             Add downloads folder \r\n\
             > scum -p /home/user/Downloads downloads \r\n\
             > scum -i game_id -n \"Nice Game Name\" -p /home/user/games/game_name -s signature add \r\n\
             > scum -i game_id install ",
        )
        .arg(Arg::with_name("cmd").help("Add, List,... ex) Scum n name p /path/to/file add"))
        .arg(
            Arg::with_name("id")
                .short("i")
                .takes_value(true)
                .help("Game id"),
        )
        .arg(
            Arg::with_name("name")
                .short("n")
                .takes_value(true)
                .help("Long name for game"),
        )
        .arg(
            Arg::with_name("path")
                .short("p")
                .takes_value(true)
                .help("System path"),
        )
        .arg(
            Arg::with_name("signature")
                .short("s")
                .takes_value(true)
                .help("Downloaded file signature"),
        )
        .get_matches();

    let mut cmd: String = String::new();
    if matches.is_present("cmd") {
        cmd = matches.value_of("cmd").unwrap().to_lowercase();
    }

    match cmd.as_str() {
        "add" => {
            let mut config = get_config();
            if config.add(
                matches.value_of("id"),
                matches.value_of("name"),
                matches.value_of("path"),
                matches.value_of("signature"),
            )? {
                config.save()?;
            }
        }
        "downloads" => {
            let mut config = get_config();
            if config.downloads(matches.value_of("path"))? {
                config.save()?;
            }
        }
        "fav" => {
            let config = get_config();
            config.favs();
        }
        "list" => {
            let config = get_config();
            config.list();
        }
        "install" => {
            let config = get_config();
            if config.install(matches.value_of("id"))? {
                config.save()?;
            }
        }
        "remove" => {
            let mut config = get_config();
            if config.remove(matches.value_of("id"), prompt("Remove scum game config?"))? {
                config.save()?;
            }
        }
        "run" => {
            let mut config = get_config();
            let id = matches.value_of("id");
            if config.set_current_process_id(id, config.run(id)?)? {
                config.save()?;
            }
        }
        "kill" => {
            let mut config = get_config();
            if config.kill(matches.value_of("id"))? {
                config.save()?;
            }
        }
        "update" => {
            let mut config = get_config();
            if config.update(
                matches.value_of("id"),
                matches.value_of("name"),
                matches.value_of("path"),
                matches.value_of("signature"),
            )? {
                config.save()?;
            }
        }
        "version" => {
            let version = env!("CARGO_PKG_VERSION");
            println!("Version: {}", version);
        }
        _ => eprintln!("Invalid commands"),
    }

    Ok(())
}
